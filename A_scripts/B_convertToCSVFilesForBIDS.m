% This script serves to create event timeseries for the task MR data.

% 180220 | written by JQK
% 181112 | added fixation regressor

% Required information: block onsets and block duration (in TRs respective to initial TR in preprocessed data)
% regressor event time series with respect to first volume in preprocessed data

clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/';
pn.dataIn       = [pn.root, 'raw/C_study/mri/behavior_eye/2*'];

%% subject IDs

% N = 44 + N = 53;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';...
    '2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';...
    '2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';...
    '2250';'2251';'2252';'2258';'2261'};

for indID = 1:numel(IDs)
    for indRun = 1:4
        try
    
        %% load regressor MATs

        load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/', ....
            IDs{indID}, '_Run', num2str(indRun),'_regressors.mat'], 'Regressors', 'RegressorInfo');

        %% convert to CSV structure
        
        Dim = sum(Regressors(:,7:10),2);
        Regressors = double(Regressors);
        RegressorTitle = {'trial_dimensionality';'block_onset'; 'trial_onset'; 'stimulus_onset'; 'block_dimensionality'; 'trial_serial_position'; 'stimulus_serial_position'; 'color_cued'; 'direction_cued'; 'size_cued'; 'luminance_cued'; 'probe_onset'; 'probe_accuracy'; 'probe_rt'; 'fixation_onset'};        
        RegressorTable = array2table(([Dim,Regressors]), 'VariableNames', RegressorTitle);
        
        % add pseudo-onset and duration columns
        
        AddRegressorTitle = {'onset', 'duration'};
        AddRegressor = repmat("n/a", size(Regressors,1),2);
        
        RegressorTable = array2table(([AddRegressor,Dim,Regressors]), 'VariableNames', [AddRegressorTitle'; RegressorTitle]);
        
        %RegressorTable = [RegressorTitle'; table2cell(RegressorTable)];
        
        %% save as CSV structure (BIDS-compatible)

        outFile = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/BIDS/B_data/sub-STSWD',IDs{indID},'/func/sub-STSWD',IDs{indID},'_task-MAT_run-0',num2str(indRun),'_events.csv'];
        
        writetable(RegressorTable,outFile,'Delimiter','\t','WriteRowNames',true);
        
        outFile_tsv = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/BIDS/B_data/sub-STSWD',IDs{indID},'/func/sub-STSWD',IDs{indID},'_task-MAT_run-0',num2str(indRun),'_events.tsv'];

        movefile(outFile, outFile_tsv);

        catch
            disp(['Error: Subject ', IDs{indID}, ' Run ', num2str(indRun)])
        end
    end % Run loop
end % ID loop